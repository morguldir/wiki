#!/usr/bin/env bash
set -euo pipefail
#
# Copyright (c) 2017 Feral Hosting. This content may not be used elsewhere without explicit permission from Feral Hosting.
#
# This script can be used to install and configure PlexPy.

# Functions

plexpyMenu () # user-friendly menu for installing the software
{
    echo
    echo -e "\033[36m""PlexPy""\e[0m"
    echo "1 Install PlexPy"
    echo "2 Restart PlexPy"
    echo "3 Re-do proxypass (if you've switched)"
    echo "4 Uninstall PlexPy"
    echo "q Quit the script"
}

cronAdd () # creates a temp cron to a variable, makes the necessary files. Each software to then check to see if job exists and add if not.
{
    tmpcron="$(mktemp)"
}

portGenerator () # generates a port to use with software installs
{
    portGen=$(shuf -i 10001-32001 -n 1)
}

portCheck () # runs a check to see if the port generated can be used
{
    while [[ "$(netstat -ln | grep ':'"$portGen"'' | grep -c 'LISTEN')" -eq "1" ]];
    do
        portGenerator;
    done
}

proxypassGenerator ()
{
    if [[ -f ~/.apache2/pid ]]
    then
        echo "Installing proxypass for Apache2..."
        if [[ -f ~/.apache2/conf.d/$software.conf ]]
        then
            rm ~/.apache2/conf.d/$software.conf
            /usr/sbin/apache2ctl -k graceful > /dev/null 2>&1
        fi
        echo "Include /etc/apache2/mods-available/proxy.load
Include /etc/apache2/mods-available/proxy_http.load
Include /etc/apache2/mods-available/headers.load

ProxyRequests Off
ProxyPreserveHost On
ProxyVia On

ProxyPass /$software http://10.0.0.1:$portGen/${USER}/$software
ProxyPassReverse /$software http://10.0.0.1:$portGen/${USER}/$software" > ~/.apache2/conf.d/$software.conf
        /usr/sbin/apache2ctl -k graceful > /dev/null 2>&1
    elif [[ -f ~/.nginx/pid ]]
    then
        echo "Installing proxypass for Nginx..."
        if [[ -f ~/.nginx/conf.d/000-default-server.d/$software.conf ]]
        then
            rm ~/.nginx/conf.d/000-default-server.d/$software.conf
            /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf > /dev/null 2>&1
        fi
        echo "location /$software {
proxy_set_header X-Real-IP \$remote_addr;
proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
proxy_set_header Host \$http_x_host;
proxy_set_header X-NginX-Proxy true;

rewrite /(.*) /$(whoami)/\$1 break;
proxy_pass http://10.0.0.1:$portGen/;
proxy_redirect off;
}" > ~/.nginx/conf.d/000-default-server.d/$software.conf
        /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf > /dev/null 2>&1
    else
        echo "Neither Apache2 nor Nginx appear to be running correctly..."
        echo
    fi
}

while [ 1 ]
do
    plexpyMenu
    echo
    read -ep "Enter the number of the option you want: " CHOICE
    echo
    case "$CHOICE" in
        "1") # install plexpy
            if [[ -d ~/plexpy ]]
            then    
                echo "PlexPy already exists and needs to be removed before reinstallation!"
                read -ep "Are you sure you want to reinstall? [y] yes or [n] no: " CONFIRM
                    if [[ $CONFIRM =~ ^[Yy]$ ]]
                    then
                        pkill -fxu $(whoami) 'SCREEN -dmS plexpy' || true
                        sleep 2 # kill PlexPy and wait
                        rm -rf ~/plexpy # remove directory
                        crontab -u $(whoami) -l | grep -v '@reboot screen -dmS plexpy python ~/plexpy/PlexPy.py' | crontab -u $(whoami) - # remove from crontab
                        echo "PlexPy has been removed."
                    else
                    echo "Taking no action..."
                    echo
                    break
                    fi
            fi
            echo "Getting PlexPy..."
            git clone https://github.com/JonnyWong16/plexpy.git
            wget -qO ~/plexpy/config.ini https://bitbucket.org/feralio/wiki/raw/HEAD/src/wiki/software/plexpy/config.ini
            echo "Configuring PlexPy..."
            portGenerator
            portCheck
            sed -i "s|http_port = 8181|http_port = $portGen|g" ~/plexpy/config.ini
            sed -i "s|http_root = \"\"|http_root = /$(whoami)/plexpy/|g" ~/plexpy/config.ini
            sed -i "s|https_domain = localhost|https_domain = $(hostname -f)|g" ~/plexpy/config.ini
            echo "Adding cron task to autostart on server reboot..."
            cronAdd
            if [[ "$(crontab -l 2> /dev/null | grep -oc '^\@reboot screen -dmS plexpy python ~/plexpy/PlexPy.py$')" == "0" ]]
            then
                crontab -l 2> /dev/null > "$tmpcron"
                echo "@reboot screen -dmS plexpy python ~/plexpy/PlexPy.py" >> "$tmpcron"
                crontab "$tmpcron"
                rm "$tmpcron"
            else
                echo "This cron job already exists in the crontab"
                echo
            fi
            echo "Setting up the ProxyPass..."
            software=plexpy
            proxypassGenerator
            echo "Starting up PlexPy..."
            screen -dmS plexpy python ~/plexpy/PlexPy.py
            echo "You have now installed PlexPy"
            echo "Access PlexPy at https://$(hostname -f)/$(whoami)/$software/"
            echo
            echo "Go through the Setup Wizard help at https://www.feralhosting.com/wiki/software/plexpy#config"
            sleep 3
            ;;
        "2") # restart plexpy
            pkill -fxu $(whoami) 'SCREEN -dmS plexpy' || true
            sleep 3
            screen -dmS plexpy python ~/plexpy/PlexPy.py
            echo "PlexPy has been restarted."
            echo "Access PlexPy at https://$(hostname -f)/$(whoami)/$software/"
            sleep 3
            ;;
        "3") # redo proxypass
            software=plexpy
            proxypassGenerator
            ;;
        "4") # uninstall plexpy
            echo -e "Uninstalling PlexPy will" "\033[31m""remove the software and the config files""\e[0m"
            read -ep "Are you sure you want to uninstall? [y] yes or [n] no: " CONFIRM
            if [[ $CONFIRM =~ ^[Yy]$ ]]
            then
                pkill -fxu $(whoami) 'SCREEN -dmS plexpy' || true
                sleep 2 # kill PlexPy and wait
                rm -rf ~/plexpy # remove directory
                crontab -u $(whoami) -l | grep -v '@reboot screen -dmS plexpy python ~/plexpy/PlexPy.py' | crontab -u $(whoami) - # remove from crontab
                echo "PlexPy has been removed."
            else
                echo "Taking no action..."
                echo
            fi
            ;;
        "q") # quit the script
            exit
            ;;
    esac
done