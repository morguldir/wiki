<h1>SickRage</h1>
<p>This article will show you how to install SickRage and configure it to be used with different torrent clients. SickRage is an automatic Video Library Manager for TV Shows.</p>
<p>You'll need to execute some commands via SSH to use this software. There is a separate guide on how to <a href="../slots/ssh">connect to your slot via SSH</a>. Commands are kept as simple as possible and in most cases will simply need to be copied and pasted into the terminal window (then executed by pressing the <kbd>Enter</kbd> key).</p>

<h2>Installation</h2>
<p>Install SickRage by logging in via SSH, then copying and pasting the following:</p>
<p><kbd>git clone https://git.sickrage.ca/SiCKRAGE/sickrage ~/.sickrage</kbd></p>

<h3>Installation notes</h3>
<p>Cloning from the new repo (<samp>git.sickrage.ca</samp>) may go slowly for you - please let the process complete.</p>

<h4>Proxypass</h4>
<p>By default, you will access the software in your browser by using the URL with a port number. If you consider the URL without a port number to be neater, you will need to <a href="../slots/proxypass">configure a proxypass</a>.</p>

<h4>Updating Unrar</h4>
<p>SickRage needs an updated version of Unrar if you need that facility in post-processing. Please see this guide to <a href="unrar">upgrading Unrar</a>.</p>

<h4>Using other repositories</h4>
<p>If you would rather use a different repository for SickRage, simply replace the URL in the installation command. Please note though that the guide assumes the repository used is as per the installation command - later commands may not work correctly if you change the repository.</p>

<h3>Configuring before starting SickRage</h3>
<aside class="alert note">The commands below assume you have <a href="pip">installed a version of Pip</a> to be able to painlessly upgrade and install modules</aside>
<p>Once you've got a copy of Pip running in a virtual environment, copy and paste the following to set up various components of SickRage:</p>
<pre><kbd>~/pip/bin/pip install --upgrade -r ~/.sickrage/requirements.txt
~/pip/bin/pip install --upgrade pyopenssl</kbd></pre>

<p>A config.ini file needs to be created and tweaked slightly before we can properly run SickRage. You can copy and paste the commands below to do this.</p>
<pre><kbd>~/pip/bin/python ~/.sickrage/SiCKRAGE.py -d --pidfile="$HOME/.sickrage/sickrage.pid"
pkill -9 -fu "$(whoami)" 'SiCKRAGE.py'
rm -rf ~/.sickrage/sickrage.pid &amp;&amp; sleep 10
mkdir ~/.sickrage.tv.shows
sed -ri 's|web_port = (.*)|web_port = '"$(shuf -i 10001-32001 -n 1)"'|g' ~/.sickrage/config.ini
sed -ri 's|web_root = (.*)|web_root = /'"$(whoami)"'/sickrage|g' ~/.sickrage/config.ini
sed -ri 's|launch_browser = 1|launch_browser = 0|g' ~/.sickrage/config.ini
sed -ri 's#root_dirs = (.*)#root_dirs = 0|'"$HOME"'/.sickrage.tv.shows#g' ~/.sickrage/config.ini</kbd></pre>

<h2>Starting, stopping and restarting</h2>
<p>This section covers the SickRage process - starting it, stopping it and restarting it. It also covers checking if the process is running, in case that becomes necessary.</p>
<p>You can start SickRage and print its URL with the following:</p>
<pre><kbd>~/pip/bin/python ~/.sickrage/SiCKRAGE.py -d --pidfile="$HOME/.sickrage/sickrage.pid" &amp;&amp; echo http://$(hostname -f):$(sed -rn 's/(.*)web_port = //p' ~/.sickrage/config.ini)/$(whoami)/sickrage</kbd></pre>
<p>If nothing happens or there is an error it could be that the port that was picked in the configuration step in use. Simply run these commands again to try with a different number (you'll need to edit the proxypass if you have one set up):</p>
<pre><kbd>sed -ri 's|web_port = (.*)|web_port = '"$(shuf -i 10001-32001 -n 1)"'|g' ~/.sickrage/config.ini
python ~/.sickrage/SickBeard.py -d --pidfile="$HOME/.sickrage/sickrage.pid"</kbd></pre>

<p>If you need to check that it's running, you can run the command below. If the process is running a list of relevant process ID numbers will be listed. If nothing is listed, the process is not running.</p>
<p><kbd>pgrep -fu "$(whoami)" "python $HOME/.sickrage/SickBeard.py -d"</kbd></p>

<p>The SickRage process is stopped by executing:</p>
<p><kbd>kill "$(pgrep -fu "$(whoami)" "python $HOME/.sickrage/SickBeard.py -d")"</kbd></p>

<p>If SickRage needs to be restarted, copy and paste this command:</p>
<pre><kbd>kill "$(pgrep -fu "$(whoami)" "python $HOME/.sickrage/SickBeard.py -d")" &amp;&amp; rm -rf ~/.sickrage/sickrage.pid &amp;&amp; ~/pip/bin/python ~/.sickrage/SickBeard.py -d --pidfile="$HOME/.sickrage/sickrage.pid"</kbd></pre>

<p>Please note that if you want to stop the process for SickRage, it's best to check it afterwards to make sure it has stopped. If the process has crashed and will not stop when requested, you can kill it with:</p>
<p><kbd>kill -9 "$(pgrep -fu "$(whoami)" "python $HOME/.sickrage/SickBeard.py -d")"</kbd></p>

<h3>Automatically restarting SickRage if it is not running</h3>
<p>Cron jobs can be used to check if SickRage is running and start it up if it is not. There is a separate page on <a href="../slots/cron">configuring cron jobs</a>.</p>

<h2>Configuring</h2>
<p>This section covers setting a password in SickRage and connecting the SickRage to different clients.</p>

<h3>Setting a password in SickRage</h3>
<p>Within the SickRage UI, click the <kbd>Config</kbd> icon (the two gears at the top) and then click <kbd>General</kbd>. Next, click the <kbd>Interface</kbd> tab. Near the bottom of the fields you'll be able to set a username and password for the web UI. Whilst it's not mandatory you do this, it is a sensible thing to do.</p>

<h3>Configuring clients with SickRage</h3>
<p>In SickRage's user interface click the <kbd>Config</kbd> icon (the two gears at the top) and then click <kbd>Search Settings</kbd>. Next, click the <kbd>Torrent Search</kbd> tab. You can choose the torrent client you want to configure from the menu.</p>

<h4>rTorrent</h4>
<p>First of all you need to create the rTorrent RPC and this is done by <a href="nginx">switching from apache to nginx</a>.</p>

<dl>
    <dt>Torrent host:port</dt>
    <dd><kbd>https://<var>server</var>.feralhosting.com/<var>user</var>/rtorrent/rpc/<kbd> (replace <var>server</var> with your server name and <var>user</var> with your username)</dd>
    <dt>Http Authentication</dt>
    <dd><kbd>Basic</kbd></dd>    
    <dt>Client username</dt>
    <dd><kbd>rutorrent</kbd> (the word 'rutorrent', <em>not</em> your ruTorrent username)</dd>
    <dt>Client password</dt>
    <dd>The same password as you use to access ruTorrent</dd>
</dl>

<h4>Deluge (via webUI)</h4>
<dl>
    <dt>Torrent host:port</dt>
    <dd>https://<var>server</var>.feralhosting.com/<var>user</var>/deluge/</dd>
    <dd>(replace <var>server</var> with your server name and <var>user</var> with your username)</dd>
    <dt>Client password</dt>
    <dd>Your Deluge webUI password</dd>    
    <dt>Downloaded files location</dt>
    <dd>Make sure it's set to a location you're happy with (or make sure it's blank to use Deluge's default).</dd>
</dl>

<h4>Deluge (via daemon)</h4>
<p>There are a couple of commands we need to run via SSH first to obtain the daemon port and password (the password is not the same as the webUI password).</p>
<p>To find the port run this command:</p>
<p><kbd>sed -rn 's/(.*)"daemon_port": (.*),/\2/p' ~/.config/deluge/core.conf</kbd></p>
<p>To find the password run this command:</p>
<p><kbd>sed -rn "s/$(whoami):(.*):(.*)/\1/p" ~/.config/deluge/auth</kbd></p>
<p>Armed with the port and password, enter the details as follows:</p>
<dl>
    <dt>Torrent host:port</dt>
    <dd>https://<var>server</var>.feralhosting.com:<var>port</var> (replace <var>server</var> with your server name and <var>port</var> with the results of the command above)</dd>
    <dt>Client username</dt>
    <dd>Your Deluge daemon username (unless you've changed it, it'll be your Feral username)</dd>    
    <dt>Client password</dt>
    <dd>Your Deluge daemon password</dd>    
    <dt>Downloaded files location</dt>
    <dd>Make sure it's set to a location you're happy with (or make sure it's blank to use Deluge's default).</dd>
</dl>

<h4>Transmission</h4>
<dl>
    <dt>Torrent host:port</dt>
    <dd>https://<var>server</var>.feralhosting.com/<var>user</var> (replace <var>server</var> with your server name and <var>user</var> with your username)</dd>
    <dt>Transmission RPC URL</dt>
    <dd><kbd>transmission/rpc</kbd></dd>    
    <dt>Client username</dt>
    <dd>The username you use to access Transmission</dd>    
    <dt>Client password</dt>
    <dd>The same password as you use to access Transmission</dd>
</dl>

<h4>SABnzbd</h4>
<p>SickRage can also search for NZB files. This section covers connecting SickRage to <a href="sabnzbd">SABnzbd</a>.</p>
<dl>
    <dt>SABnzbd server URL</dt>
    <dd>https://<var>server</var>.feralhosting.com:<var>port</var>/sabnzbd/ (replace <var>server</var> with your server name and <var>port</var> with your HTTPS port for SABnzbd)</dd>    
    <dt>SABnzbd username</dt>
    <dd>The username you set for the SABnzbd UI</dd>    
    <dt>SABnzbd password</dt>
    <dd>The password you set for the SABnzbd UI</dd>    
    <dt>SABnzbd API key</dt>
    <dd>Find this in the <samp>General</samp> section of SABnzbd's settings</dd>
</dl>

<h2>Troubleshooting</h2>
<p>If you experience issues or crashes using SickRage the first thing to try and do is restart the software using the command listed in the section above, <i>Starting, stopping and restarting</i>.</p>
<dl>
    <dt>When trying to install the requirements, I get <samp>bash: <var>$HOME</var>/pip/bin/pip: No such file or directory</samp></dt>
    <dd><p>Make sure you've <a href="pip">installed your own version of Pip</a></p></dd>
    <dt>When trying to run SickRage, I get <samp>Failed to import required libs...</samp></dt>
    <dd><p>Please make sure you've a) installed a version of Pip to your slot (see above) and b) are calling Python using <samp>~/pip/bin/python</samp> instead of just <samp>python</samp></p></dd>
    <dt>When I click <kbd>Test Connection</kbd> I get the error <samp>Error: Unable to connect to rTorrent'</samp></dt>
    <dd><p>If rTorrent is running and you've made the <a href="nginx">switch to nginx</a> this error likely means some of the details you've entered are not correct. Please double-check that your username is rutorrent (as in the actual word, "rutorrent", not the username you use to log in to ruTorrent). If you changed your ruTorrent password after switching to nginx, you'll need to use the old password. If you no longer recall it, please reinstall nginx.</p></dd>
    <dt>I went to start up SickRage and received an error <samp>'...sickrage.pid already exists. Exiting.'</samp></dt>
    <dd>
        <p>Assuming you're using the default locations please run the following command before trying the start up command again:</p>
        <p><kbd>rm ~/.sickrage/sickrage.pid</kbd></p>
    </dd>
    <dt>When I use pip I get an error <samp>IndexError: list index out of range</samp></dt>
    <dd><p>Please try using the option <kbd>--no-use-wheel</kbd> when using pip.</p></dd>
</dl>

<h2>Uninstallation</h2>
<aside class="alert note">The commands below will completely remove the software and its config files - back up important data first!</aside>
<pre><kbd>kill -9 "$(pgrep -fu "$(whoami)" "python $HOME/.sickrage/SickBeard.py -d")"
rm -rf ~/.sickrage ~/.sickrage.tv.shows</kbd></pre>

<h2>External links</h2>
<ul>
    <li><a href="https://sickrage.github.io/">SickRage's homepage</a></li>
</ul>