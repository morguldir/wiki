#!/usr/bin/env bash
set -euo pipefail
#
# Copyright (c) 2017 Feral Hosting. This content may not be used elsewhere without explicit permission from Feral Hosting.
#
# This script can be used to install Jackett and its dependencies. It can also restart and uninstall it.

# Functions

jackettMenu ()
{
    echo -e "\033[36m""Jackett""\e[0m"
    echo "1 Install Jackett"
    echo "2 Restart Jackett"
    echo "3 Uninstall Jackett"
    echo "b Go back"
    echo "q Quit the script"
}

binCheck () # checks for ~/bin
{
    if [[ ! -d ~/bin ]]
    then
        echo "Creating ~/bin first"
        mkdir -p ~/bin
    fi
}

cmakeInstall ()

{
    binCheck
    echo "Downloading CMake..."
    wget -qO ~/cmake-temp.tar.gz https://cmake.org/files/v3.9/cmake-3.9.2-Linux-x86_64.tar.gz
    echo "Extracting and configuring..."
    mkdir -p ~/cmake-temp
    tar xf ~/cmake-temp.tar.gz --strip-components=1 -C ~/cmake-temp
    cp -rf ~/cmake-temp/{bin,share} ~/ 
    rm -rf ~/cmake-temp*
    echo "You have now installed $(~/bin/cmake --version | head -n 1)"
    echo
}

cronAdd () # creates a temp cron to a variable, makes the necessary files. Each software to then check to see if job exists and add if not.
{
    tmpcron="$(mktemp)"
}

monoInstall ()
{
    if [[ ! -f ~/bin/cmake ]]
    then
        cmakeInstall
    fi
    if [[ ! -f ~/bin/libtool ]]
    then
        echo "Mono needs libtool - getting it now..."
        wget -qO ~/libtool.tar.gz http://ftpmirror.gnu.org/libtool/libtool-2.4.6.tar.gz
        tar xf ~/libtool.tar.gz && cd ~/libtool-2.4.6
        echo "Installing libtool..."
        ./configure --prefix=$HOME > /dev/null 2>&1
        make > /dev/null 2>&1
        make install > /dev/null 2>&1 
        cd && rm -rf libtool{-2.4.6,.tar.gz}
        echo "You have now installed $(~/bin/libtool --version 2>&1 | head -n 1)"
    fi
    echo "Getting Mono..."
    PATH=~/bin:$PATH
    wget -qO ~/mono.tar.bz2 http://download.mono-project.com/sources/mono/mono-5.4.0.167.tar.bz2
    tar xf ~/mono.tar.bz2 && cd ~/mono-5.4.0.167
    echo "Compiling Mono. It can take around 30 minutes to do - please be patient..." && sleep 2
    ./autogen.sh --prefix="$HOME"
    make get-monolite-latest && make && make install # output left so user knows something is still happening
    cd && rm -rf ~/mono{-*,.tar.bz2}
    echo "You have now installed mono $(~/bin/mono --version 2>&1 | head -n 1 | awk -F 'compiler | \(' '{print $2}')"
}

portGenerator () # generates a port to use with software installs
{
    portGen=$(shuf -i 10001-32001 -n 1)
}

portCheck () # runs a check to see if the port generated can be used
{
    while [[ "$(netstat -ln | grep ':'"$portGen"'' | grep -c 'LISTEN')" -eq "1" ]];
    do
        portGenerator;
    done
}

proxypassGenerator ()
{
    if [[ -f ~/.apache2/pid ]]
    then
        echo "Installing proxypass for Apache2..."
        if [[ -f ~/.apache2/conf.d/$software.conf ]]
        then
            rm ~/.apache2/conf.d/$software.conf
            /usr/sbin/apache2ctl -k graceful > /dev/null 2>&1
        fi
        echo "Include /etc/apache2/mods-available/proxy.load
Include /etc/apache2/mods-available/proxy_http.load
Include /etc/apache2/mods-available/headers.load

ProxyRequests Off
ProxyPreserveHost On
ProxyVia On

ProxyPass /$software http://10.0.0.1:$portGen/${USER}/$software
ProxyPassReverse /$software http://10.0.0.1:$portGen/${USER}/$software" > ~/.apache2/conf.d/$software.conf
        /usr/sbin/apache2ctl -k graceful > /dev/null 2>&1
    elif [[ -f ~/.nginx/pid ]]
    then
        echo "Installing proxypass for Nginx..."
        if [[ -f ~/.nginx/conf.d/000-default-server.d/$software.conf ]]
        then
            rm ~/.nginx/conf.d/000-default-server.d/$software.conf
            /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf > /dev/null 2>&1
        fi
        echo "location /$software {
proxy_set_header X-Real-IP \$remote_addr;
proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
proxy_set_header Host \$http_x_host;
proxy_set_header X-NginX-Proxy true;

rewrite /(.*) /$(whoami)/\$1 break;
proxy_pass http://10.0.0.1:$portGen/;
proxy_redirect off;
}" > ~/.nginx/conf.d/000-default-server.d/$software.conf
        /usr/sbin/nginx -s reload -c ~/.nginx/nginx.conf > /dev/null 2>&1
    else
        echo "Neither Apache2 nor Nginx appear to be running correctly..."
        echo
    fi
}

while [[ 1 ]]
do
    echo
    jackettMenu
    echo
    read -ep "Enter the number of the option you want: " CHOICE
    echo
    case "$CHOICE" in
        "1") # install Jackett
            if [[ ! -f ~/bin/mono ]]
            then
                echo "Jackett needs mono. Installing everything needed..."
                monoInstall
                echo
            fi
            echo "Getting Jackett..."
            wget -qO ~/Jackett.tar.gz https://github.com/Jackett/Jackett/releases/download/v0.8.147/Jackett.Binaries.Mono.tar.gz
            echo "Extracting and configuring Jackett..."
            tar xf ~/Jackett.tar.gz
            rm ~/Jackett.tar.gz
            mkdir -p ~/.config/Jackett
            portGenerator
            portCheck
            echo "{
  \"Port\": $portGen,
  \"AllowExternal\": true,
  \"APIKey\": \"\",
  \"AdminPassword\": null,
  \"InstanceId\": \"$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 64 | head -n 1)\",
  \"BlackholeDir\": null,
  \"UpdateDisabled\": false,
  \"UpdatePrerelease\": false,
  \"BasePathOverride\": \"/$(whoami)/jackett\",
  \"OmdbApiKey\": null
}" > ~/.config/Jackett/ServerConfig.json
            echo "Adding cron task to autostart on server reboot..."
            cronAdd
            if [[ "$(crontab -l 2> /dev/null | grep -oc "^\@reboot screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/bin/mono --debug ~/Jackett/JackettConsole.exe'$")" == "0" ]]
            then
                crontab -l 2> /dev/null > "$tmpcron"
                echo "@reboot screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/bin/mono --debug ~/Jackett/JackettConsole.exe'" >> "$tmpcron"
                crontab "$tmpcron"
                rm "$tmpcron"
            else
                echo "This cron job already exists in the crontab"
                echo
            fi
            echo "Setting up the ProxyPass..."
            software=jackett
            proxypassGenerator
            echo "Starting up Jackett..."
            screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/bin/mono --debug ~/Jackett/JackettConsole.exe'
            echo "You have now installed $(~/bin/mono --debug ~/Jackett/JackettConsole.exe --version | tail -n 1)"
            echo "Access Jackett at https://$(hostname -f)/$(whoami)/$software/"
            echo
            ;;
        "2") # restart Jackett            
            if [[ ! -f ~/bin/mono ]]
            then
                echo "Jackett needs mono. Please rerun the installation part of the script"
                break                
            else
                echo "Restarting Jackett..."
                pkill -fxu $(whoami) 'SCREEN -dmS jackett' || true
                screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/bin/mono --debug ~/Jackett/JackettConsole.exe'
            fi
            ;;
        "3") # uninstall jackett
            echo -e "Uninstalling Jackett will" "\033[31m""remove the software and the config files""\e[0m"
            read -ep "Are you sure you want to uninstall? [y] yes or [n] no: " CONFIRM
            if [[ $CONFIRM =~ ^[Yy]$ ]]
            then
                pkill -fxu $(whoami) 'SCREEN -dmS jackett' || true
                sleep 2 # kill jackett and wait
                rm -rf ~/Jackett ~/.config/Jackett # remove directories
                crontab -u $(whoami) -l | grep -v "@reboot screen -dmS jackett /bin/bash -c 'export TMPDIR=~/tmp; ~/bin/mono --debug ~/Jackett/JackettConsole.exe'" | crontab -u $(whoami) - # remove from crontab
                echo "Jackett has been removed."
            else
                echo "Taking no action..."
                echo
            fi
            ;;
        "b") # go back to main menu
            break
            ;;
        "q") # quit the script entirely
            exit
            ;;
    esac
done